const Estudiante = require("./estudiante");

const objEstudiante = new Estudiante("Juan", "Hernandez", "juan@gmail.com", "987654321");

objEstudiante.saludar();

console.log( objEstudiante.sumar(5.333, 2.3456) );
/*
objEstudiante.setNombre("Pedro");
objEstudiante.setApellido("Quintana");
objEstudiante.setEmail("pedro@gmail.com");
objEstudiante.setTelefono("123456789");
*/

console.log(objEstudiante);
console.log( objEstudiante.email );

objEstudiante.matricularMateria("m001", "Matemáticas");
objEstudiante.getMateria("m001").nota = 1;
objEstudiante.matricularMateria("m002", "Filosofía");
objEstudiante.getMateria("m002").nota = 3;
objEstudiante.matricularMateria("m003", "Quimica");
objEstudiante.getMateria("m003").nota = 5;
objEstudiante.matricularMateria("m004", "Fisica");
objEstudiante.getMateria("m004").nota = 7;

objEstudiante.mostrarMaterias();

console.log( objEstudiante.getMateria("m002") );

console.log( objEstudiante.elevarCuadrado() );

objEstudiante.cancelarMateria("m003");
objEstudiante.mostrarMaterias();