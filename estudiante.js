const Materia = require("./materia");

class Estudiante{

    constructor(nombre, apellido, email, telefono){
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.telefono = telefono;
        this.materias = [];
        console.log("Método constructor de Estudiante");
    }

    setNombre(nombre){
        this.nombre = nombre;
    }

    setApellido(apellido){
        this.apellido = apellido;
    }

    setEmail(email){
        this.email = email;
    }

    setTelefono(telefono){
        this.telefono = telefono;
    }

    saludar(){
        console.log("HOLA MUNDO");
    }

    sumar(num1, num2){
        return num1 + num2;
    }

    matricularMateria(codigo, nombre){
        let objMateria = new Materia(codigo, nombre);
        this.materias.push(objMateria);
    }

    mostrarMaterias(){
        this.materias.forEach(element=>{
            console.log(element.codigo +" - "+ element.nombre);
        });
    }

    getMateria(codigo){
        let objMateria = this.materias.filter( m => (m.codigo == codigo) );
        return objMateria[0];
    }

    elevarCuadrado(){
        let tempo = this.materias.map( m =>{
            m.nota = Math.pow(m.nota, 2);
            return m;
        });
        return tempo;
    }

    cancelarMateria(codigo){
        let tempo = [];
        this.materias.forEach(element => {
            if(element.codigo != codigo){
                tempo.push(element);
            }
        });
        this.materias = tempo;
    }

}

module.exports = Estudiante;

/*****************************************************************
 * Cree una clase Materia con los siguientes
 * atributos: codigo, nombre, nota
 * Desarrolle una serie de métodos que permitan 
 * al estudiante matricular una materia, 
 * MOSTRAR todas las materias y retornar una materia 
 * por su código.
 * codigo - nombre_matreria
 *****************************************************************/